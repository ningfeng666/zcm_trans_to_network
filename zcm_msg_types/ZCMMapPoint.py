"""ZCM type definitions
This file automatically generated by zcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class ZCMMapPoint(object):
    __slots__ = ["position_x", "position_y"]

    IS_LITTLE_ENDIAN = False;
    def __init__(self):
        self.position_x = 0.0
        self.position_y = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(ZCMMapPoint._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">ff", self.position_x, self.position_y))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != ZCMMapPoint._get_packed_fingerprint():
            raise ValueError("Decode error")
        return ZCMMapPoint._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = ZCMMapPoint()
        self.position_x, self.position_y = struct.unpack(">ff", buf.read(8))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if ZCMMapPoint in parents: return 0
        tmphash = (0x15e6ca5fd1bcb28f) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff)  + ((tmphash>>63)&0x1)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if ZCMMapPoint._packed_fingerprint is None:
            ZCMMapPoint._packed_fingerprint = struct.pack(">Q", ZCMMapPoint._get_hash_recursive([]))
        return ZCMMapPoint._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

