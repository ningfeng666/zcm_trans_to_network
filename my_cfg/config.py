#!/usr/bin/env python3
# -*-coding:utf-8 -*-
'''
@File    :   config.py
@Time    :   2021/11/14 19:35:08
@Author  :   littleHuang USTC 
@Version :   1.0
@Contact :   ysh626.std@gmail.com
@License :   (C)Copyright 2020-2021, USTC
@Desc    :   ZCM数据转发工具配置文件
'''


class MyConfig:
    PORT = 7788  # 默认监听端口，建议全局统一
    ADDR = '0.0.0.0'  # 默认监听IP
    BROADCAST_ADDR = "255.255.255.255"  # 广播地址
    CHANNELS = [  # 需要使用的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
        ("TEST", "HELLO"), ("TEST2", "WORLD")
    ]

    # host A
    LISTEN_CHANNELS = [  # 需要侦听并转发到网络的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
        ("TEST", "HELLO")
    ]
    # host B
    # LISTEN_CHANNELS = [  # 需要侦听的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
    #     ("TEST2", "WORLD")
    # ]
