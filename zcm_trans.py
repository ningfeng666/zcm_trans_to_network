#!/usr/bin/env python3
# -*-coding:utf-8 -*-
'''
@File    :   zcm_trans.py
@Time    :   2021/11/14 20:41:51
@Author  :   littleHuang USTC 
@Version :   1.0
@Contact :   ysh626.std@gmail.com
@License :   (C)Copyright 2020-2021, USTC
@Desc    :   主程序，由此启动
'''

from my_cfg.config import MyConfig
from trans_tools.net2zcm import Net2ZCM
from trans_tools.zcm2net import ZCM2Net
from trans_tools.socket_tools import SocketTools
from zerocm import ZCM
from multiprocessing import Process

if __name__ == '__main__':
    st = SocketTools(MyConfig.ADDR, MyConfig.PORT, MyConfig.BROADCAST_ADDR)
    zcm = ZCM("ipc")
    z2n = ZCM2Net(st, zcm, MyConfig.LISTEN_CHANNELS)
    n2z = Net2ZCM(st, zcm, MyConfig.CHANNELS)
    Process(target=z2n.main).start()
    n2z.main()
    # threading.Thread(target=z2n.main).start()
    # threading.Thread(target=n2z.main).start()

