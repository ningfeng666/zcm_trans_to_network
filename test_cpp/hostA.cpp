#include <unistd.h>
#include <zcm/zcm-cpp.hpp>
#include "HELLO.hpp"
#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
    zcm::ZCM zcm {"ipc"};
    if (!zcm.good())
        return 1;

    std::string channel = "TEST";

    HELLO my_data {};
    my_data.str = "Hello!";
    if (argc<2){
        cout<<"please input the aimIP"<<endl;
        return 0;
    }
    else
        my_data.addr = argv[1];
    while (1) {
        zcm.publish(channel, &my_data);
        usleep(1000*1000);
    }

    return 0;
}