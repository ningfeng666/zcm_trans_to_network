#include <stdio.h>
#include <unistd.h>
#include <string>
#include <zcm/zcm-cpp.hpp>
#include "HELLO.hpp"
using std::string;

class Handler
{
    public:
        ~Handler() {}

        void handleMessage(const zcm::ReceiveBuffer* rbuf,
                           const string& chan,
                           const HELLO *msg)
        {
                printf("Received a message on channel '%s'\n", chan.c_str());
                printf("Received a message from ip '%s'\n", msg->addr.c_str());
                printf("msg->str = '%s'\n", msg->str.c_str());
        }
};

int main(int argc, char *argv[])
{
    zcm::ZCM zcm {"ipc"};
    if (!zcm.good())
        return 1;

    Handler handlerObject;
    zcm.subscribe("TEST", &Handler::handleMessage, &handlerObject);
    zcm.run();

    return 0;
}