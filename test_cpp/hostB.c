#include <stdio.h>
#include <zcm/zcm.h>
#include "HELLO.h"

void callback_handler(const zcm_recv_buf_t *rbuf, const char *channel, const HELLO *msg, void *usr)
{
    printf("Received a message on channel '%s'\n", channel);
    printf("Received a message from ip '%s'\n", msg->addr);
    printf("msg->str = '%s'\n", msg->str);
    printf("\n");
}

int main(int argc, char *argv[])
{
    zcm_t *zcm = zcm_create("ipc");
    HELLO_subscribe(zcm, "TEST", callback_handler, NULL);

    zcm_run(zcm);

    // Can also call zcm_start(zcm); to spawn a new thread that calls zcm_run(zcm);
    //
    // zcm_start(zcm)
    // while(!done) { do stuff; sleep; }
    // zcm_stop(zcm);
    //

    zcm_destroy(zcm);
    return 0;
}
