#include <unistd.h>
#include <zcm/zcm.h>
#include "HELLO.h"

int main(int argc, char *argv[])
{
    zcm_t *zcm = zcm_create("ipc");
    HELLO msg;
    msg.str = (char*)"Hello, World!";
    if (argc<2){
        printf("please input the aimIP");
        return 0;
    }
    else
        msg.addr = argv[1];

    while (1) {
        HELLO_publish(zcm, "TEST", &msg);
        usleep(1000000); /* sleep for a second */
    }

    zcm_destroy(zcm);
    return 0;
}