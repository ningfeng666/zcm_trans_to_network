from zerocm import ZCM
import sys
sys.path.insert(0, './')
from zcm_msg_types.WORLD import WORLD
import time

zcm_t = ZCM("ipc")

if not zcm_t.good():
    print("Unable to initialize zcm")
    exit()

send_msg = WORLD()

send_msg.str = "World!"
send_msg.addr = sys.argv[1]
while True:
    zcm_t.publish("TEST2", send_msg)
    time.sleep(5)




