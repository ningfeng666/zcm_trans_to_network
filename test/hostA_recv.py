from zerocm import ZCM
import sys
sys.path.insert(0, './')
from zcm_msg_types.WORLD import WORLD
import time

zcm_t = ZCM("ipc")

if not zcm_t.good():
    print("Unable to initialize zcm")
    exit()

def handler(channel, msg):
    print(F"recv {msg.str} on channel {channel}")


subs = zcm_t.subscribe("TEST2", WORLD, handler)
while True:
    zcm_t.handle()

