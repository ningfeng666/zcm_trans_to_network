from zerocm import ZCM
import sys
sys.path.insert(0, './')
from zcm_msg_types.HELLO import HELLO
import time

zcm_t = ZCM("ipc")

if not zcm_t.good():
    print("Unable to initialize zcm")
    exit()

send_msg = HELLO()

send_msg.str = "Hello!"
send_msg.addr = sys.argv[1]
while True:
    zcm_t.publish("TEST", send_msg)
    time.sleep(5)




