#!/usr/bin/env python3
# -*-coding:utf-8 -*-
'''
@File    :   socket_tools.py
@Time    :   2021/11/14 20:41:29
@Author  :   littleHuang USTC 
@Version :   1.0
@Contact :   ysh626.std@gmail.com
@License :   (C)Copyright 2020-2021, USTC
@Desc    :   套接字相关
'''
import socket
import struct
from my_cfg.config import MyConfig

for package in MyConfig.CHANNELS:
    exec("from zcm_msg_types.%s import %s" % (package[1], package[1]))


class SocketTools:
    def __init__(self, addr, port, broadcast_addr, aim_port=None):
        """
        @description : 
        ---------
        @param msg_classes : 所有消息类名，便于导入
        @param addr : 本机绑定的IP
        @param port : 本机绑定的端口
        @param aimport : 目标主机的端口，默认与本机端口相同
        -------
        @Returns : 
        -------
        """

        self.udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_sock.bind((addr, port))
        self.broad_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.broad_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.broadcast_addr = broadcast_addr
        self.aim_port = aim_port if aim_port else port

    def udp_send(self, msg_type, msg):
        """
        @description : 接收网络消息
        ---------
        @param msg_type : 消息类型
        @param msg : 消息对象
        -------
        @Returns : 
        -------
        """

        msg_byte = msg.encode()
        msg_type_byte = msg_type.encode('utf-8')
        buf = bytes([len(msg_type_byte)]) + msg_type_byte + msg_byte
        addr = msg.addr.split(':')
        ip = addr[0]
        if len(addr) == 1:
            port = self.aim_port
        else:
            port = int(addr[1])
        if ip == '255.255.255.255':  # 0代表广播
            self.broad_sock.sendto(buf, (self.broadcast_addr, port))
        else:
            self.udp_sock.sendto(buf, (ip, port))

    def udp_recv(self):
        """
        @description : 发送网络消息
        ---------
        @param : 
        -------
        @Returns msg_type : 消息类型
        @Returns msg_type : 消息对象
        -------
        """

        buf, addr = self.udp_sock.recvfrom(65535)
        msg_type_len = int(buf[0])
        msg_start = msg_type_len + 1
        msg_type = buf[1:msg_start].decode('utf-8')
        msg = eval('%s.decode(buf[msg_start:])' % msg_type)  # 执行语句并获取返回值
        msg.addr = '%s:%s' % addr
        return msg_type, msg

    def tcp_send(self):
        pass

    def tcp_recv(self):
        pass
