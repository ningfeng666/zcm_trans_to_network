#!/usr/bin/env python3
# -*-coding:utf-8 -*-
'''
@File    :   net2zcm.py
@Time    :   2021/11/14 20:38:48
@Author  :   littleHuang USTC 
@Version :   1.0
@Contact :   ysh626.std@gmail.com
@License :   (C)Copyright 2020-2021, USTC
@Desc    :   将网络数据转化为zcm
'''


class Net2ZCM:
    def __init__(self, recv_sock, send_zcm, CHANNELS):
        """
        @description : 
        ---------
        @param recv_sock : 接收网络信息的socket
        @param send_zcm : 本地发送消息的zcm
        @param CHANNELS : 频道及消息类型对应       
        -------
        @Returns : 
        -------
        """

        self.sock = recv_sock
        self.zcm = send_zcm
        self.channelsDic = {}  # 有类型到频道的映射
        for channel, msg_type in CHANNELS:
            self.channelsDic[msg_type] = channel

    def main(self):
        """
        @description : 转发主函数
        ---------
        @param : 
        -------
        @Returns : 
        -------
        """
        while True:
            msg_type, msg = self.sock.udp_recv()
            channel = self.channelsDic[msg_type]
            self.zcm.publish(channel, msg)
