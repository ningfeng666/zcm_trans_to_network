#!/usr/bin/env python3
# -*-coding:utf-8 -*-
'''
@File    :   zcm2net.py
@Time    :   2021/11/14 20:41:05
@Author  :   littleHuang USTC 
@Version :   1.0
@Contact :   ysh626.std@gmail.com
@License :   (C)Copyright 2020-2021, USTC
@Desc    :   将本机zcm数据转发到网络
'''
from my_cfg.config import MyConfig

for package in MyConfig.CHANNELS:
    exec("from zcm_msg_types.%s import %s" % (package[1], package[1]))


class ZCM2Net:
    def __init__(self, send_sock, recv_zcm, CHANNELS):
        """
        @description : 
        ---------
        @param recv_sock : 接收网络信息的socket
        @param send_zcm : 本地接收消息的zcm
        @param CHANNELS : 频道及消息类型对应
        -------
        @Returns : 
        -------
        """

        self.sock = send_sock
        self.zcm = recv_zcm
        self.channelsDic = {}  # 由频道到类型的映射
        for channel, msg_type in CHANNELS:
            self.channelsDic[channel] = msg_type

    def handler(self, channel, msg):
        msg_type = self.channelsDic[channel]
        self.sock.udp_send(msg_type, msg)

    def main(self):
        """
        @description : 转发主函数
        ---------
        @param : 
        -------
        @Returns : 
        -------
        """
        for channel, msg_type in self.channelsDic.items():
            exec("self.zcm.subscribe('%s',%s , self.handler)" % (channel, msg_type))
        while True:
            self.zcm.handle()
