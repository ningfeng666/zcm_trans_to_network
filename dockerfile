FROM zerocm/zcm
MAINTAINER littleHuang<ysh626.std@gmail.com>
ENV MYPATH /zcm_trans
COPY my_cfg/ $MYPATH/my_cfg/
COPY test/ $MYPATH/test/
COPY test_cpp/ $MYPATH/test_cpp/
COPY trans_tools/ $MYPATH/trans_tools/
COPY zcm_msg_types/  $MYPATH/zcm_msg_types/
COPY zcm_trans.py $MYPATH/
RUN cp /zcm/build/zcm/python/zerocm.cpython-35m-x86_64-linux-gnu.so /usr/lib/python3/dist-packages/
ENV LD_LIBRARY_PATH /usr/lib:/usr/local/lib
WORKDIR $MYPATH/test_cpp
RUN mkdir build&& g++ hostA.cpp -std=c++11 -lzcm -o build/hostA && g++ hostB.cpp -std=c++11 -lzcm -o build/hostB
WORKDIR $MYPATH
EXPOSE 7788