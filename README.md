## 1. 功能说明

zcm转发模块，负责将本地zcm数据转发到网络中，以及接收网络中的zcm数据并转发到对应频道。

本地进程间通信依赖zcm ipc，网络进程间通信依赖于udp socket。

## 2. 使用说明

### 2.1 原有模块改动

本模块为独立进程，故仅需要对原有模块作细微改动，以适应本模块。

具体改动为：在原有各种消息的定义(zcm文件)中添加一个类型为字符串的`addr字段`，如下例所示；

```c
struct HELLO
{
    string str;
    string addr;
}
```

addr可以说 `a.b.c.d`的形式指定IP，也可以是`a.b.c.d:port` 的形式指定IP以及端口

这个addr用于表示消息的目的IP地址(及端口)或源IP地址及端口——**发送时，用于告知本模块接收方的IP地址；接收时，携带发送方的IP地址。发送时，当此地址被标志为`255.255.255.255`时，代表目标IP是广播**。



### 2.2 消息类型定义文件

消息类型文件统一存放在 本模块路径`/zcm_msg_types/`文件夹下，消息类型文件通过使用命令`zcm-gen -p xxxx.zcm`生成。

### 2.3 配置文件填写

配置文件路径为：本模块路径`/my_cfg/config.py` 内容如下：

```python
class MyConfig:
    PORT = 7788  # 默认监听端口，建议全局统一
    ADDR = '0.0.0.0'  # 默认监听IP
    BROADCAST_ADDR = "255.255.255.255"  # 广播地址
    CHANNELS = [  # 需要使用的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
        ("TEST", "HELLO"), ("TEST2", "WORLD")
    ]

    # host A
    LISTEN_CHANNELS = [  # 需要侦听并转发到网络的频道及对应的消息类型
        ("TEST", "HELLO")
    ]
```

其中CHANNELS为一个tuple列表，每个tuple由需要**使用**（收或发）的频道及其对应的消息类型组成。

LISTEN_CHANNELS列表与CHANNELS列表格式相同，但只需要添加需要本模块侦听（即需要本模块转发到网络中）的频道。

> 注：由于zcm工具多个进程在同一个频道上收发时经常会有获取锁失败提示，虽然暂未发现问题，但不建议在同一个频道上同时收发

### 2.4 依赖

~~要求python版本为3.6及以上(支持f-string)~~，安装zcm模块并配置好python依赖。暂未使用其他第三方库。

### 2.5 执行

配置完成后，在本模块路径下，执行命令 `python3 zcm_trans.py`即可。

## 3. 快速开始

### 3.1 本地环境配置

本模块预设了测试组件，用以帮助您快速尝试，请事先准备两台可以相互ping同的主机或虚拟机。

1. 需要用到的zcm文件为

   ```c
   // hello.zcm
   struct HELLO
   {
       string str;
       string addr;
   }
   ```

   ```c
   // world.zcm
   struct WORLD
   {
       string str;
       string addr;
   }
   ```

   将生成好的HELLO.py 和 WORLD.py 放在zcm_msg_types目录下。

2. 主机A的配置文件如下

   ```python
   class MyConfig:
       PORT = 7788  # 默认监听端口，建议全局统一
       ADDR = '0.0.0.0'  # 默认监听IP
       BROADCAST_ADDR = "255.255.255.255"  # 广播地址
       CHANNELS = [  # 需要使用的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
           ("TEST", "HELLO"), ("TEST2", "WORLD")
       ]
   
       # host A
       LISTEN_CHANNELS = [  # 需要侦听并转发到网络的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
           ("TEST", "HELLO")
       ]
   ```

3. 配置完成后，在主机A上依次执行

   ```shell
   python3 test/hostA_send.py [主机B的IP] &	# 启动主机A发送进程
   python3 zcm_trans.py	# 启动转发进程
   python3 test/hostA_recv.py	# 启动主机A的接收进程
   ```

4. 主机B的配置文件如下

   ```python
   class MyConfig:
       PORT = 7788  # 默认监听端口，建议全局统一
       ADDR = '0.0.0.0'  # 默认监听IP
       BROADCAST_ADDR = "255.255.255.255"  # 广播地址
       CHANNELS = [  # 需要使用的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
           ("TEST", "HELLO"), ("TEST2", "WORLD")
       ]
   
       # host B
       LISTEN_CHANNELS = [  # 需要侦听的频道及对应的消息类型，其中消息类型为type文件夹中定义的各种消息
           ("TEST2", "WORLD")
       ]
   ```

5. 配置完成后，在主机B上依次执行

   ```shell
   python3 test/hostB_send.py [主机A的IP] &	# 启动主机B发送进程
   python3 zcm_trans.py &	# 启动转发进程
   python3 test/hostB_recv.py	# 启动主机B的接收进程
   ```

顺利执行后，每隔5秒，主机B会受到来自主机A的 Hello！，主机A会收到来自主机B的 World！

### 3.2 使用dockerfile

1. 编译镜像

   ```sh
   # 在项目目录中执行
   docker build -t littlehuang/zcm_trans .
   ```

2. 创建docker网络并启动容器

   ```sh
   # 创建docker网络
   docker network create --driver bridge --subnet 10.0.10.0/24 --gateway 10.0.10.1 zcm_trans_test
   # 启动并进入第一个容器
   docker run --name zcm_trans01 --net zcm_trans_test -it littlehuang/zcm_trans /bin/bash
   # 新建一个终端，启动并进入第二个容器
   docker run --name zcm_trans02 --net zcm_trans_test -it littlehuang/zcm_trans /bin/bash
   ```

3. 执行并测试

   ```sh
   # 在第二个终端中启动转发和接收程序
   python3 zcm_trans.py >/dev/null 2>&1 &
   ./test_cpp/build/hostB
   # 在第一个终端中启动转发和发生程序
   python3 zcm_trans.py >/dev/null 2>&1 &
   ./test_cpp/build/hostA zcm_trans02
   ```

4. 效果如图

![image-20211213143314459](README.assets/image-20211213143314459.png)

### 3.3 使用docker时的跨节点通信

#### 3.3.1 两个主机的容器均使用主机网络

1. 在两台主机上分别编译镜像

   ```sh
   # 在项目目录中执行
   docker build -t littlehuang/zcm_trans .
   ```

2. 在两台主机分别以主机网络模式启动容器

   ```sh
   docker run --name zcm_trans --net host -it littlehuang/zcm_trans /bin/bash
   ```

3. 在两台主机的容器端启动转发程序，并分别启动zcm发送程序和接收程序

   ```sh
   # 主机2
   python3 zcm_trans.py >/dev/null 2>&1 &
   ./test_cpp/build/hostB
   # 主机1
   python3 zcm_trans.py >/dev/null 2>&1 &
   ./test_cpp/build/hostA 主机2的IP
   
   ```

#### 3.3.2 容器使用docker虚拟网络和主机网络代理

1. 在两台主机上分别编译镜像

   ```sh
   # 在项目目录中执行
   docker build -t littlehuang/zcm_trans .
   ```

2. 在两台主机分别以虚拟网络模式启动容器，并制定代理端口

   ```sh
   # 主机1
   docker run --name zcm_trans -p 7777:7788/udp -it littlehuang/zcm_trans /bin/bash
   # 主机2
   docker run --name zcm_trans -p 8888:7788/udp -it littlehuang/zcm_trans /bin/bash
   ```

3. 在两台主机的容器端启动转发程序，并分别启动zcm发送程序和接收程序

   ```sh
   # 主机2
   python3 zcm_trans.py >/dev/null 2>&1 &
   ./test_cpp/build/hostB
   # 主机1
   python3 zcm_trans.py >/dev/null 2>&1 &
   ./test_cpp/build/hostA 主机2的IP:8888
   
   ```

## 4. 版本记录

### v1.0 

作者：黄勇胜 USTC

时间：2021年11月16日

功能改动：完成了基本的zcm数据编解码和转发功能

### v1.1

作者：黄勇胜 USTC

时间：2021年12月13日

功能改动：

1. 环境依赖为python3即可不再需要3.6以上
2. 增加了c/cpp的测试程序

### v1.2

作者：黄勇胜 USTC

时间：2021年12月13日

功能改动：添加了dockerfile

### v1.3

作者：黄勇胜 USTC

时间：2021年12月14日

功能改动：

1. 动态端口支持，addr字段携带目的ip之外也可以携带目的端口
2. 添加了跨主机docker通信的演示

